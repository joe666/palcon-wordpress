#!/bin/bash

REPOS="animal bert bigbird camilla casino consumersreviews cookiemonster ernie fozzie kermit oscar robin sports"
REPO_BASE=git@bitbucket.org-luismolinaxlm:webpals/

for r in $REPOS; do 
    git clone $REPO_BASE$r.git themes/$r
done

exit 0
